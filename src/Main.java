import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] array = {"5+6+32=43",
                "1+1=2",
                "(3+5)*4*21=483",
                "((21-20)*(32-30))/2=1"};
        System.out.println("Initial array: " + Arrays.toString(array));
        System.out.println("--------");
        selectSort(array);
        System.out.println("Sorted array: " + Arrays.toString(array));
    }

    public static void selectSort(String[] array) {
        for (int i = 0; i < array.length; i++) {
            String min = array[i];
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (countChar(array[j]) < countChar(min)) {
                    min = array[j];
                    minIndex = j;
                }
            }
            swap(array, i, minIndex);
        }
    }

    public static int countChar(String string) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '+' || string.charAt(i) == '-' || string.charAt(i) == '*' || string.charAt(i) == '/') {
                count++;
            }
        }
        return count;
    }

    public static void swap(String[] array, int a, int b) {
        String temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}